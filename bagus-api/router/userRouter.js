const express = require('express')
const router = express.Router()
const { getAll, addUser, delUser, signInUser, getProfile } = require('../controller/userController')

router.get('/', getAll)
router.post('/signup', addUser)
router.delete('/:userId', delUser)
router.post('/signin', signInUser)
router.get('/:userId', getProfile)

module.exports = router
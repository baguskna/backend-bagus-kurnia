const express = require('express')
const app = express()
const router = require('./router')

app.use(express.json())

require('./config/db')()


app.use('/api', router)


app.listen(3000, () => console.log('PORT 3000'))

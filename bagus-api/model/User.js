const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  gender: {type: String, required: [true, 'Choose your gender'], emum: ['Male', 'Female', 'Other']},
  name: { 
    firstName: { type: String, required: [true, "Why you don't have name?"]},
    lastName: { type: String }
  }
})

const User = new mongoose.model('users', userSchema)

exports.User = User;
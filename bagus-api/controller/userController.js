const { User } = require('../model/User');
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

async function getAll(req, res) {
  User.find()
    .sort({ created: -1 })
    .select('-__v')
    .exec()
    .then(users => {
      res.status(200).json({
        success: true,
        message: "Done",
        users
      })
    })
    .catch(err => {
      res.status(500).json({
        success: false,
        message: "An error occured",
        err
      })
    })
}

addUser = (req, res) => {
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return res.status(409).json({
          message: 'Email exists'
        });
      } else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err
            });
          } else {
            const user = new User({
              _id: new mongoose.Types.ObjectId(),
              name: {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
              },
              gender: req.body.gender,
              email: req.body.email,
              password: hash
            });
            user.save()
              .then(result => {
                res.status(201).json({
                  message: 'User created',
                  result: {
                    name: `${result.name.firstName} ${result.name.lastName}`,
                    email: result.email,
                    gender: result.gender
                  }
                });
              })
              .catch(err => {
                res.status(500).json({
                  error: err
                })
              })
          }
        });
      }
    })
};

delUser = (req, res) => {
  User.remove({ _id: req.params.userId })
    .exec()
    .then(result => {
      res.status(200).json({
        message: 'User Deleted'
      });
    })
    .catch(err => {
      res.status(500).json({
        error: err
      })
    })
}

signInUser = async (req, res, next) => {
  const userExist = await User.findOne({ email: req.body.email });
  const { password } = req.body;
  
  if (!userExist) {
    res.status(404).json({
      success: false,
      message: "Email incorrect"
    });
  } else if (!password || password === "") {
    res.status(400).json({
      success: false,
      message: "Password must be filled"
    })
  }

  bcrypt.compare(req.body.password, userExist.password)
    .then(result => {
      if (result) {
        const token = jwt.sign(userExist.toJSON(), process.env.JWT_KEY, {
          algorithm: 'HS256',
          expiresIn: '12h'
        })
        res.setHeader('Authorization', token)

        return res.status(200).json({
          success: true,
          message: "Login success",
          token: token
        })
      } else {
        res.status(401).json({
          success: false,
          message: "Password incorrect",
          status: 401
        })
      }
    })
    .catch(err => {
      next(err)
    })
}


getProfile = (req, res) => {
  User.findOne({ _id: req.params.userId })
    .select('-password -__v')
    .exec()
    .then(result => {
      res.status(200).json({
        success: true,
        message: 'Done',
        result: result
      })
    })
    .catch(err => {
      res.status(500).json({
        success: false,
        message: 'An error occured',
        status: 500,
        error: err
      })
    })
}

module.exports = { getAll, addUser, delUser, signInUser, getProfile }